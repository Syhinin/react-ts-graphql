import * as ReactDOM from 'react-dom';
import * as React from 'react';
import { Route, Router } from "react-router-dom";
import theme from './components/ui-elements/theme'
import { MuiThemeProvider } from '@material-ui/core/styles';


import './styles/App.css';

import { Auth0Provider } from "./pages/Auth/react-auth0-spa";
import { AUTH_CONFIG } from './pages/Auth/auth0-variables';
import history from "./utils/history";

const onRedirectCallback = (appState: any) => {
  history.push(
    appState && appState.targetUrl
      ? appState.targetUrl
      : window.location.pathname
  );
};

const mainRoutes = (

  <MuiThemeProvider theme={theme}>
    <Router history={history}>
      <Route path="/" render={props => (
        <Auth0Provider
          domain={AUTH_CONFIG.domain}
          client_id={AUTH_CONFIG.clientId}
          redirect_uri={AUTH_CONFIG.callbackUrl}
          onRedirectCallback={onRedirectCallback} />
      )} />
    </Router>
  </MuiThemeProvider>
);

ReactDOM.render(mainRoutes, document.getElementById("root"));
