import { createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import red from '@material-ui/core/colors/red';

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
    color: '#fff',
  },
  palette: {
    primary: {
      main: '#E535AB'
    },
    secondary: blue,
    error: red,
    contrastThreshold: 111,
    tonalOffset: 1,
    contrastText: '#fff',
  }
});

export default theme;

