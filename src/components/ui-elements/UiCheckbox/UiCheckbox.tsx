
import React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import Checkbox from '@material-ui/core/Checkbox';
import { styles } from './styles'

function HigherOrderComponent(props: WithStyles<typeof styles>) {
  const [checked, setChecked] = React.useState(true);
  const { classes } = props;

  const handleChange = (event: any) => {
    setChecked(event.target.checked);
  }
  return <Checkbox color='primary' size='medium' className={classes.root} checked={checked} onChange={handleChange} />
}

export default withStyles(styles)(HigherOrderComponent);

