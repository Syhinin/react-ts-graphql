import { createStyles } from '@material-ui/core/styles';

export const styles = ({ palette }) => createStyles({
  root: {
    color: palette.primary.main,
    margin: '10px',
  },
});