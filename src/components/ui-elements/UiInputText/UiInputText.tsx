import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { styles } from './styles'

export interface PropTypes {
  id: string
  label: string,
  classes: any
}

function HigherOrderComponent(props: PropTypes) {
  const { classes, id, label } = props;
  return <TextField className={classes.root} id={id} label={label} variant="filled" />;
}

export default withStyles(styles)(HigherOrderComponent);