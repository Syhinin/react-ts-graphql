import React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { styles } from './styles'

function HigherOrderComponent(props: WithStyles<typeof styles>) {
  const { classes } = props;
  return <Button  className={classes.root}>Click me, please!</Button>;
}

export default withStyles(styles)(HigherOrderComponent);