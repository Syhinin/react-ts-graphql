import { createStyles } from '@material-ui/core/styles';

export const styles = ({ palette }) => createStyles({
  root: {
    background: palette.primary.main,
    border: 0,
    borderRadius: 3,
    color: 'white',
    fontSize: '20px',
    height: 40,
    padding: '0 24px',
    margin: '10px',
  }
});