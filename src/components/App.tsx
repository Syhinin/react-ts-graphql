import * as React from 'react';
import { Container } from '@material-ui/core';


import ApolloClient from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { WebSocketLink } from 'apollo-link-ws';
import { ApolloProvider } from '@apollo/react-hooks';

import UiCheckbox from './ui-elements/UiCheckbox/UiCheckbox'
import UiButton from './ui-elements/UiButton/UiButton'
import UiInputText from './ui-elements/UiInputText/UiInputText'

import { useAuth0 } from "../pages/Auth/react-auth0-spa";

const createApolloClient = (authToken: string) => {
  return new ApolloClient({
    link: new WebSocketLink({
      uri: 'wss://hasura.io/learn/graphql',
      options: {
        reconnect: true,
        connectionParams: {
          headers: {
            Authorization: `Bearer ${authToken}`
          }
        }
      }
    }),
    cache: new InMemoryCache(),
  });
};

const App = ({ idToken }: { idToken: string }) => {
  const { loading, logout } = useAuth0();
  if (loading || !idToken) {
    return (<div>Loading...</div>);
  }
  const client = createApolloClient(idToken);
  return (
    <ApolloProvider client={client}>
      <Container >

        <h1>main mage</h1>
        <UiCheckbox /><br />
        <UiButton /><br />
        <UiInputText label='text input' id='1232132' />
      </Container>
    </ApolloProvider>
  );
};

export default App;
