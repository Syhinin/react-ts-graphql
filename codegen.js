module.exports = {
    "schema": [
        {
            "https://hasura.io/learn/graphql": {
                "headers": {
                    "Authorization": "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik9FWTJSVGM1UlVOR05qSXhSRUV5TURJNFFUWXdNekZETWtReU1EQXdSVUV4UVVRM05EazFNQSJ9.eyJodHRwczovL2hhc3VyYS5pby9qd3QvY2xhaW1zIjp7IngtaGFzdXJhLWRlZmF1bHQtcm9sZSI6InVzZXIiLCJ4LWhhc3VyYS1hbGxvd2VkLXJvbGVzIjpbInVzZXIiXSwieC1oYXN1cmEtdXNlci1pZCI6ImF1dGgwfDVlOTcwOWI4NmU2M2M1MGMxMzQ0ZTU3YyJ9LCJuaWNrbmFtZSI6InN5aGluaW4iLCJuYW1lIjoic3loaW5pbkBnbWFpbC5jb20iLCJwaWN0dXJlIjoiaHR0cHM6Ly9zLmdyYXZhdGFyLmNvbS9hdmF0YXIvMGZiYmFiMzI0NzlmZWIzNGY4YTViZWYxMDgyMGU5NDE_cz00ODAmcj1wZyZkPWh0dHBzJTNBJTJGJTJGY2RuLmF1dGgwLmNvbSUyRmF2YXRhcnMlMkZzeS5wbmciLCJ1cGRhdGVkX2F0IjoiMjAyMC0wNC0xNVQxNjowNjoxMi43MjVaIiwiaXNzIjoiaHR0cHM6Ly9ncmFwaHFsLXR1dG9yaWFscy5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NWU5NzA5Yjg2ZTYzYzUwYzEzNDRlNTdjIiwiYXVkIjoiUDM4cW5GbzFsRkFRSnJ6a3VuLS13RXpxbGpWTkdjV1ciLCJpYXQiOjE1ODY5NjY4MDksImV4cCI6MTU4NzAwMjgwOSwiYXRfaGFzaCI6InFrRkhVQ1VJRjNTQjBwMjJTOGpNaGciLCJub25jZSI6Im44MU9UMWxZemFrLkF6Q2RBTi51emxoOHN3aWRhZGQyIn0.i2mqLXW6MpI8f1skumoInv2i8dI8421rkw-CxSCqsuTzn2f7We7nNIeKay8IGufuqd8xuRyD_mBLpJFTBsiz4w2ux8yxv6dlIJClrC9JacFiWcY4pxSDPg3LFLP6SLPLKoMTOD23CH8X6kkX_T8-W2BcQAQOO2Rug-dhzX5IGmFCsmDN0OZmDxRcSdmPfIKUqWYACvKlCjjTEgcjtaH67aa2KagYTMZZdMapwZ7piGXhydzqPYphb8pGDMmdVFCaUxIOtSYK3mWXD9IvTKrJY_KzPvUicnU6Jow9B8jBxWEwkrY6XbUD1o7AJdv7TRifebTHE0yTIgDxTWs6ahRyig"
                }
            }
        }
    ],
    "documents": [
        "./src/**/*.tsx",
        "./src/**/*.ts"
    ],
    "overwrite": true,
    "generates": {
        "./src/generated/graphql.tsx": {
            "plugins": [
                "typescript",
                "typescript-operations",
                "typescript-react-apollo"
            ],
            "config": {
              "skipTypename": false,
              "withHooks": true,
              "withHOC": false,
              "withComponent": false
            }
        },
        "./graphql.schema.json": {
          "plugins": [
                "introspection"
          ]
        }
    }
};
